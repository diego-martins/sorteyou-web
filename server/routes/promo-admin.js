var express = require('express');
var router = express.Router();
var auth = require('../core/auth');
var firebase = require('firebase-admin');
var moment = require('moment');
var utils = require('../core/utils');
var promoDao = require('../data/dao/promo');
var userDao = require('../data/dao/user');
var idFormat = '([a-f0-9\d]{24})';
var dateFormat = ["YYYY-MM-DD"];

router.get(`/admin/promo/:id${idFormat}`, auth.authenticated, function (req, res, next) {

    var promoId = req.params.id;
    var userId = req.user.id;

    promoDao.get(promoId).then(result => {
        if(!result){
            return res.json({success: false, message: res.__('promo_not_found')});
        }

        if(result.userId != userId) {
            return res.status(401).send(res.__('access_denied'));
        }

        return res.json({
            success: true,
            data: result
        });

    }).catch(next);

});

router.get('/admin/promo', auth.authenticated, function (req, res, next) {
    
    var userId = req.user.id;

    promoDao.getList(userId).then(result => {
        return res.json({
            success: true,
            data: result
        });
    }).catch(next);

});

router.get('/admin/promo/following', auth.authenticated, function (req, res, next) {
    
    var userId = req.user.id;

    promoDao.getListFollowing(userId).then(result => {
        return res.json({
            success: true,
            data: result
        });
    }).catch(next);

});

router.get(`/admin/promo/:id${idFormat}/export-users`, auth.authenticated, function (req, res, next) {

    var promoId = req.params.id;
    var userId = req.user.id;

    promoDao.get(promoId).then(promoResult => {

        if(!promoResult) {
            return res.json({
                success: false, 
                message: res.__('promo_not_found')
            });
        }

        let isOwner = (promoResult.userId == userId);
        if(!isOwner){
            return res.json({
                success: false,
                message: res.__('user_not_allowed')
            });                
        }
        
        let isFinished = moment().isAfter(moment(promoResult.finishAt));
        if (!isFinished) {
            return res.json({
                success: false,
                message: res.__('promo_not_completed')
            });
        }

        return promoDao.getWinners(promoId).then(winnersResult => {

            return userDao.getByPromo(promoId).then(usersResult => {
                var list = [];
                if(usersResult){
                    usersResult.forEach(item => {
                        var won = false;
                        if(winnersResult){
                            winnersResult.forEach(winnerItem => {
                                if(String(winnerItem) == String(item.id)) won = true;
                            });    
                        }
                        list.push({
                            name: item.name,
                            email: item.email,
                            won: won 
                        });
                    });
                }
                return res.json(list);
            });        
        });

    }).catch(next);

});

router.post('/admin/promo', auth.authenticated, function (req, res, next) {

    var userId = req.user.id;
    var form = req.body;
    form.id = undefined;
    form.file = req.files.length ? req.files[0] : undefined;
    var formErrors = hasFormErrors(form, res.__);

    if (formErrors) {
        utils.deleteFile(form.file);
        return res.json({
            success: false,
            data: {
                errors: formErrors
            }
        });
    }

    var model = {
        id: promoDao.getId(),
        name: form.name,
        desc: form.desc,
        terms: form.terms,
        userId: userId,
        startAt: moment(form.startAt, dateFormat).toDate(),
        finishAt: moment(form.finishAt, dateFormat).toDate(),
        image: ''
    };

    var bucket = firebase.storage().bucket();
    var dest = 'promos/' + model.id + '.jpg';
    bucket.upload(form.file.path, {
        destination: dest,
        metadata: {
            contentType: form.file.mimetype,
            metadata: {
                firebaseStorageDownloadTokens: model.id
            }
        }
    }).then(uploadeds => {

        utils.deleteFile(form.file);
        var uploaded = uploadeds[0];
        model.image = "https://firebasestorage.googleapis.com/v0/b/" + uploaded.metadata.bucket + "/o/" + encodeURIComponent(dest) + "?alt=media&token=" + model.id;

        return promoDao.add(model).then(result => {
            return res.json({
                success: true,
                data: {
                    id: result
                }
            });
        });

    }).catch(err => {
        utils.deleteFile(form.file);
        next(err);
    });
});

router.put(`/admin/promo/:id${idFormat}`, auth.authenticated, function (req, res, next) {

    var userId = req.user.id;
    var promoId = req.params.id;
    var form = req.body;
    form.id = promoId;
    form.file = req.files.length ? req.files[0] : undefined;
    
    var formErrors = hasFormErrors(form, res.__);

    if (formErrors) {
        utils.deleteFile(form.file);
        return res.json({
            success: false,
            data: {
                errors: formErrors
            }
        });
    }

    promoDao.get(promoId).then(result => {
        
        if(!result){
            utils.deleteFile(form.file);
            return res.json({success: false, message: res.__('promo_not_found')});
        }

        if(String(result.userId) != String(userId)){
            utils.deleteFile(form.file);
            return res.status(401).send(res.__('access_denied'));
        }

        var model = {
            id: promoId,
            name: form.name,
            desc: form.desc,
            terms: form.terms,
            startAt: result.startAt, //startAt never change after defined
            finishAt: moment(form.finishAt, dateFormat).toDate()
        };

        if (form.file) {
            var bucket = firebase.storage().bucket();
            var dest = 'promos/' + model.id + '.jpg';
            return bucket.upload(form.file.path, {
                destination: dest,
                metadata: {
                    contentType: form.file.mimetype,
                    metadata: {
                        firebaseStorageDownloadTokens: model.id
                    }
                }
            }).then(uploadeds => {
                utils.deleteFile(form.file);
                var uploaded = uploadeds[0];
                model.image = "https://firebasestorage.googleapis.com/v0/b/" + uploaded.metadata.bucket + "/o/" + encodeURIComponent(dest) + "?alt=media&token=" + model.id;
                return promoDao.update(model).then(result => {
                    return res.json({
                        success: true,
                        data: {
                            id: result
                        }
                    });
                });
            });
        } else {
            return promoDao.update(model).then(result => {
                return res.json({
                    success: true,
                    data: {
                        id: result
                    }
                });
            });        
        }
    }).catch(err => {
        utils.deleteFile(form.file);
        next(err);
    });
    
});

var hasFileError = function (form, __) {
    const maxSize = 500 * 1024; //bytes = 500KB
    if (!form.id && !form.file) {
        return __('error_select_image');
    } else if (form.file && form.file.mimetype != 'image/jpeg') {
        return __('error_select_image_type', {type: 'JPEG'});
    } else if (form.file && form.file.size > maxSize) {
        return __('error_select_image_size', {size: '500KB'});
    }
    return undefined;
};

var hasFormErrors = function (form, __) {
    var errors = [];

    if (!form.name) {
        errors.push(__('error_require_name'));
    } else if (form.name.length > 50) {
        errors.push(__('error_name_size', {size: 50}));
    }

    let isValidDates = true;
    if (!form.startAt) {
        errors.push(__('error_require_start_date'));
        isValidDates = false;
    } else if (
        form.startAt.length != 10 ||
        !moment(form.startAt, dateFormat, true).isValid()
    ) {
        errors.push(__('error_invalid_start_date'));
        isValidDates = false;
    }

    if (!form.finishAt) {
        errors.push(__('error_require_finish_date'));
        isValidDates = false;
    } else if (
        form.finishAt.length != 10 || !moment(form.finishAt, dateFormat, true).isValid()
    ) {
        errors.push(__('error_invalid_finish_date'));
        isValidDates = false;
    }

    if(isValidDates){
        if(moment(form.startAt, dateFormat).isSame(moment(form.finishAt, dateFormat).toDate())){
            errors.push(__('error_dates_cannot_equals'));
        }
        else if(moment(form.startAt, dateFormat).isAfter(moment(form.finishAt, dateFormat))){
            errors.push(__('error_dates_order'));
        }
    }

    let fileError = hasFileError(form, __);
    if (fileError) {
        errors.push(fileError);
    }

    if (!form.desc) {
        errors.push(__('error_require_desc'));
    } else if (form.desc.length > 500) {
        errors.push(__('error_desc_size', {size: 500}));
    }

    if (!form.terms) {
        errors.push(__('error_require_terms'));
    } else if (form.terms.length > 500) {
        errors.push(__('error_terms_size', {size: 500}));
    }

    return errors.length ? errors : undefined;
};

var getRandom = function (minVal, maxVal) {
    return Math.floor(Math.random() * (maxVal - minVal + 1)) + minVal;
};

router.put(`/admin/promo/:id${idFormat}/good-luck`, auth.authenticated, function (req, res, next) {

    var userId = req.user.id;
    var promoId = req.params.id;    
    
    promoDao.get(promoId).then(promoResult => {
        if(!promoResult){
            return res.json({success: false, message: res.__('promo_not_found')});
        }

        if(promoResult.userId != userId){
            return res.status(401).send(res.__('access_denied'));
        }
        
        if(promoResult.isCompleted){
            return res.json({
                success: false,
                message: res.__('promo_completed')
            })                
        }

        let isFinished = moment().isAfter(moment(promoResult.finishAt));
        if(!isFinished){
            return res.json({
                success: false,
                message: res.__('promo_running')
            });
        }

        let maxWinners = promoResult.maxWinners || 1;
        if(promoResult.totalWinners >= maxWinners){
            return res.json({
                success: false,
                message: res.__('error_max_winners')
            });
        }

        return promoDao.getUsers(promoId).then(usersResult => {
            if(usersResult == undefined || usersResult.length == 0){
                return res.json({
                    success: false,
                    message: res.__('no_users')
                });
            }
            
            let userCount = usersResult.length;
            if(userCount == 0){
                return res.json({
                    success: false,
                    message: res.__('no_users')
                })
            }else{
                let nums = [];
                let winners = [];
                let maxWinners = promoResult.maxWinners || 1;
                while(nums.length < maxWinners){
                    let num = getRandom(0, userCount-1);
                    let exists = false;
                    for(let i=0; i<nums.length; i++){
                        if(nums[i] == num){
                            exists = true;
                            break;
                        }
                    }
                    if(!exists){
                        nums.push(num);
                    }
                }
                for(let i=0; i<nums.length; i++){
                    winners.push(usersResult[nums[i]]);
                }
                return promoDao.addWinners(promoId, winners).then(result => {
                    return res.json({
                        success: true,
                        data: {
                            winners: winners
                        }
                    });        
                });
            }
        });
    }).catch(next);

});

router.delete(`/admin/promo/:promoId${idFormat}/winner/:userId${idFormat}`, auth.authenticated, function (req, res, next) {

    var promoId = req.params.promoId;
    var winnerId = req.params.userId;
    var userId = req.user.id;

    promoDao.get(promoId).then(promoResult => {

        if(!promoResult) {
            return res.json({
                success: false, 
                message: res.__('promo_not_found')
            });
        }

        let isOwner = (promoResult.userId == userId);
        if(!isOwner){
            return res.json({
                success: false,
                message: res.__('user_not_allowed')
            });                
        }
        
        /*
        let isFinished = moment().isAfter(moment(promoResult.finishAt));
        if (isFinished) {
            return res.json({
                success: false,
                message: res.__('promo_completed')
            });
        }
        */

        return promoDao.hasWinner(promoId, winnerId).then(hasResult => {

            if(!hasResult) {
                return res.json({success: false, message: res.__('user_not_found')});
            }

            return promoDao.removeWinner(promoId, winnerId).then(removeResult => {
        
                if(removeResult == undefined) {
                    return res.json({success: false, message: res.__('error_remove_user')});
                }
        
                return res.json({
                    success: true,
                    data: removeResult
                });
        
            });

        });

    }).catch(next);

});

module.exports = router;