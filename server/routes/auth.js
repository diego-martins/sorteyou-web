var express = require('express');
var router = express.Router();
var auth = require('../core/auth');
var userDao = require('../data/dao/user');

router.post('/auth', function (req, res, next) {
    var gfaToken = req.body.gfaToken;
    auth.verifyFirebaseAuthToken(gfaToken).then(authResult => {
        var user = authResult
        var model = {
            name: user.name,
            email: user.email,
            picture: user.picture,
            authWith: user.firebase.sign_in_provider,
            authId: user.uid
        };
    
        return userDao.addOrUpdate(model).then(userResult => {
            return auth.createToken({uid: userResult.id}).then(tokenResult => {                
                res.json({
                    success: true,
                    data: {
                        token: tokenResult,
                        user: {
                            id: userResult.id || '',
                            name: userResult.name || '',
                            email: userResult.email || '',
                            picture: userResult.picture || ''
                        }
                    }
                });    
            });
        });
    }).catch(next);
    
});

module.exports = router;