var express = require('express');
var router = express.Router();
var auth = require('../core/auth');
var moment = require('moment');
var promoDao = require('../data/dao/promo');
var idFormat = '([a-f0-9\d]{24})';

router.get(`/promo/:id${idFormat}`, auth.loadUser, function (req, res, next) {

    const promoId = req.params.id;
    const userId = (req.user) ? req.user.id || '' : '';

    promoDao.get(promoId).then(result => {     

        if(!result){
            return res.json({
                success: false, 
                message: res.__('promo_not_found')
            });
        }

        let isOwner = (result.userId == userId);
        let isFinished = moment().isAfter(result.finishAt);
        result.isOwner = isOwner;
        result.isFinished = isFinished;    

        return res.json({
            success: true, 
            data: result
        });

    }).catch(next);
});

router.post(`/promo/:id${idFormat}/has-user`, auth.authenticated, function (req, res, next) {

    var userId = req.user.id;
    var promoId = req.params.id;
    promoDao.hasUser(promoId, userId).then(result => {
        return res.json({ 
            success: true, 
            data: result 
        });
    }).catch(next);
    
});

router.post(`/promo/:id${idFormat}/add-user`, auth.authenticated, function (req, res, next) {

    var promoId = req.params.id;
    var userId = req.user.id;

    promoDao.get(promoId).then(promoResult => {

        if(!promoResult) {
            return res.json({
                success: false, 
                message: res.__('promo_not_found')
            });
        }

        let isOwner = (promoResult.userId == userId);
        let isFinished = moment().isAfter(moment(promoResult.finishAt));
        if(isOwner){
            return res.json({
                success: false,
                message: res.__('user_not_allowed')
            });                
        }
        
        if (isFinished) {
            return res.json({
                success: false,
                message: res.__('promo_completed')
            });
        }

        if(promoResult.maxUsers && promoResult.totalUsers >= promoResult.maxUsers){
            return res.json({
                success: false,
                message: res.__('error_max_users')
            });
        }

        return promoDao.hasUser(promoId, userId).then(hasResult => {

            if(hasResult == undefined) {
                return res.json({success: false, message: res.__('user_not_found')});
            }

            if(hasResult) {
                return res.json({
                    success: false,
                    message: res.__('user_already_added')
                });
            }

            return promoDao.addUser(promoId, userId).then(addResult => {
        
                if(addResult == undefined) {
                    return res.json({success: false, message: res.__('error_add_user')});
                }
        
                return res.json({
                    success: true,
                    data: addResult
                });
        
            });

        });

    }).catch(next);

});

router.get(`/promo/:id${idFormat}/total-users`, function (req, res, next) {

    var promoId = req.params.id;
    promoDao.getTotalUsers(promoId).then(result => {
        return res.json({
            success: true,
            data: result
        });
    }).catch(next);

});

router.get(`/promo/:id${idFormat}/total-winners`, function (req, res, next) {

    var promoId = req.params.id;
    promoDao.getTotalWinners(promoId).then(result => {
        return res.json({
            success: true,
            data: result
        });
    }).catch(next);
    
});

module.exports = router;