var fs = require('fs');

module.exports = {
    deleteFile: function(file){
        let fileName = (typeof file == 'object') ? file.path : file;
        if(fileName){
            fs.exists(fileName, function(exists){
                if(exists){
                    try{
                        fs.unlink(fileName);
                    }catch(e){                    
                    }
                }
            });    
        }
    }
};