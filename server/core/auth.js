var express = require('express');
var router = express.Router();
var admin = require('firebase-admin');
var userDao = require('../data/dao/user');
var jwt = require('jsonwebtoken');
var config = require('config');

var _self = {};

_self.createToken = function(payload) {
    return new Promise((resolve, reject) => {
        if(!payload) payload = {};
        var options = {
            expiresIn: '24h'
        };
        jwt.sign(
            payload, 
            config.secret,
            options,
            (err, token) => {
                if(err){
                    return reject(err);
                }
                resolve(token);
            }
        );
    });
};

_self.verifyToken = function(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, config.secret, (err, payload) => {
            if(err){
                return reject(err);
            }
            resolve(payload);
        });
    });
};

_self.verifyFirebaseAuthToken = function(token) {
    return new Promise((resolve, reject) => {
        admin.auth().verifyIdToken(token)
        .then(result => {
            resolve(result);
        }).catch(err => {
            reject(err);
        });
    });
};

_self.authenticated = function (req, res, next) {
    var _auth = req.headers['authorization'];
    if (!_auth) {
        return res.status(401).send(res.__('access_denied'));
    }
    var _parts = _auth.split(' ');
    if (_parts.length != 2 || _parts[0].toLowerCase() != 'bearer') {
        return res.status(401).send(res.__('access_denied'));
    }
    var token = _parts[1];
    _self.verifyToken(token).then(tokenResult => {
        req.user = {
            id: tokenResult.uid
        };    
        return next();
    }).catch(err => {       
        return res.status(401).send(res.__('invalid_token')); 
    });
};

_self.loadUser = function(req, res, next) {
    if(req.user){
        return next();            
    }
    var _auth = req.headers['authorization'];
    if (!_auth) {            
        return next();
    }
    var _parts = _auth.split(' ');
    if (_parts.length != 2 || _parts[0].toLowerCase() != 'bearer') {
        return next();
    }
    var token = _parts[1];

   _self.verifyToken(token).then(tokenResult => {
        req.user = {
            id: tokenResult.uid
        };    
        return next();
    }).catch(err => {
        return next(); 
    });
};

module.exports = _self;