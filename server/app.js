var express = require('express');
var path = require('path');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var firebase = require("firebase-admin");
var multer = require('multer');
var cors = require('cors');
var i18n = require('i18n');
var mongoose = require('mongoose');
var config = require('config');
var winston = require('./config/winston');

var promo = require('./routes/promo');
var promoAdmin = require('./routes/promo-admin');
var auth = require('./routes/auth');

var serviceAccount = require(__dirname + "/serviceAccountKey.json");

var ref = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: config.firebase.databaseUrl,
  storageBucket: config.firebase.storageBucket
});

i18n.configure({
  locales: ['pt-br'],
  defaultLocale: 'pt-br',
  directory: __dirname + '/locales'
});

mongoose.Promise = global.Promise;
mongoose.connect(config.dbConnection).then(result => {
  console.log('mongodb connection succesfull');
}).catch(err => {
  console.error(err);
});

var app = express();
app.use(i18n.init);
app.use(cors());

app.use(multer({dest: __dirname + '/temp/'}).any());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'hbs');
app.engine('html', require('hbs').__express);
app.set('view engine', 'html');

app.use(morgan('combined', { stream: winston.stream }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', auth);
app.use('/api', promo);
app.use('/api', promoAdmin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  winston.info(`${404} - Not Found - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  res.status(404);
  res.send(res.__('error_404'));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  winston.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  if (res.headersSent) {
    return next(err);
  }
  res.status(err.status || 500);
  res.send(res.__('error_500'));
});

module.exports = app;
