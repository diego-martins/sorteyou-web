var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var promo = new Schema({
    name: String,
    desc: String,
    terms: String,
    image: String,
    startAt: { type: Date },
    finishAt: { type: Date },
    maxUsers: { type: Number, default: 10000 },
    maxWinners: { type: Number, default: 1 },
    userId: {type: Schema.ObjectId},
    createdAt: { type: Date, default: Date.now },
    modifiedAt: { type: Date, default: Date.now },
    users: [{type: Schema.ObjectId, ref: 'users'}],
    winners: [{type: Schema.ObjectId, ref: 'users'}]
});

module.exports = mongoose.model('Promo', promo, 'promos');