var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var user = new Schema({
    name: String,
    email: String,
    picture: String,
    authId: String,
    authWith: String,
    createdAt: { type: Date, default: Date.now },
    modifiedAt: { type: Date, default: Date.now },
    promos: [{type: Schema.ObjectId, ref: 'promos'}],
    won: [{type: Schema.ObjectId, ref: 'promos'}]    
});

module.exports = mongoose.model('User', user, 'users');