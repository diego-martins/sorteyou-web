var moment = require('moment');
var mongoose = require('mongoose');
var User = require('../models/user');

var dao = {};

dao.getId = function () {
   return mongoose.Types.ObjectId();
};

dao.get = function (id) {
    return new Promise((resolve, reject) => {
        User.findById(id, (err, result) => {
            if(err){
                reject(err);
            }else{
                resolve(toModel(result));
            }
        });
    });
};

dao.getByAuth = function (authId) {
    return new Promise((resolve, reject) => {
        User.findOne({'authId': authId}, (err, result) => {
            if(err){
                reject(err);
            }else{
                resolve(toModel(result));
            }
        });
    });
};

dao.getByPromo = function (promoId) {
    return new Promise((resolve, reject) => {
        User.find({promos: promoId}, (err, result) => {
        //User.find({}, (err, result) => {
            if(err){
                reject(err);
            }else{
                resolve(toModels(result));
            }
        });
    });
};

dao.add = function (model) {
    return new Promise((resolve, reject) => {        
        
        var doc = toDoc(model);

        User.create(doc, (err, result) => {
            if(err){
                reject(err);
            }else{
                resolve(toModel(result));
            }
        });

    });
};

dao.update = function (model) {
    return new Promise((resolve, reject) => {        
        
        var doc = toDoc(model);

        User.findByIdAndUpdate(model.id, doc, (err, result) => {
            if(err){
                reject(err);
            }else{
                resolve(toModel(result));
            }
        });
    });
};

dao.addOrUpdate = function (model) {
    return new Promise((resolve, reject) => {
        dao.getByAuth(model.authId).then((result, err) => {
            if(err){                
                return reject(err);
            }
            if(result == undefined){
                dao.add(model).then((result, err) => {
                    if(err) {
                        reject(err);
                    }else{
                        resolve(result);
                    }
                });
            }
            else {
                model.id = result.id;
                dao.update(model).then((result, err) => {
                    if(err){
                        reject(err);
                    }else{
                        resolve(result);
                    }
                });
            }
        });
    });
};

var toModel = function (doc) {
    var model;
    if(doc){
        model = {};
        if(doc._id) model.id = String(doc._id);
        if(doc.name) model.name = doc.name;
        if(doc.email) model.email = doc.email;
        if(doc.picture) model.picture = doc.picture;
        if(doc.authId) model.authId = doc.authId;
        if(doc.authWith) model.authWith = doc.authWith;
        if(doc.createdAt) model.createdAt = doc.createdAt;
        if(doc.modifiedAt) model.modifiedAt = doc.modifiedAt;
    }
    return model;
};

var toModels = function (docs) {
    var models = [];
    if(docs){
        docs.forEach(doc => {
            var model = {};
            if(doc._id) model.id = String(doc._id);
            if(doc.name) model.name = doc.name;
            if(doc.email) model.email = doc.email;
            if(doc.picture) model.picture = doc.picture;
            if(doc.authId) model.authId = doc.authId;
            if(doc.authWith) model.authWith = doc.authWith;
            if(doc.createdAt) model.createdAt = doc.createdAt;
            if(doc.modifiedAt) model.modifiedAt = doc.modifiedAt;
            if(model != {}) {
                models.push(model);
            }    
        });
    }
    return models;
};

var toDoc = function (model) {
    var doc;
    if(model){
        doc = {};
        if(model.name) doc.name = model.name;
        if(model.email) doc.email = model.email;
        if(model.picture) doc.picture = model.picture;
        if(model.authId) doc.authId = model.authId;
        if(model.authWith) doc.authWith = model.authWith;
        if(!model.id) {
            doc.createdAt = moment().toDate();
        }
        doc.modifiedAt = moment().toDate();
    }
    return doc;
};

module.exports = dao;