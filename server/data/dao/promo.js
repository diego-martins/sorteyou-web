var moment = require('moment');
var mongoose = require('mongoose');
var Promo = require('../models/promo');
var User = require('../models/user');

var dao = {};

dao.getId = function () {
   return mongoose.Types.ObjectId();
};

dao.get = function (id) {
    return new Promise((resolve, reject) => {
        Promo.findById(id, (err, result) => {
            if(err){
                return reject(err);
            }
            if(!result){
                return resolve();
            }
            resolve({
                id: String(result._id),
                name: result.name,
                desc: result.desc,
                terms: result.terms,
                startAt: result.startAt,
                finishAt: result.finishAt,
                image: result.image,
                userId: result.userId,
                maxUsers: result.maxUsers,
                maxWinners: result.maxWinners,
                totalUsers: result.users.length,
                totalWinners: result.winners.length
            });
        });
    });
};

dao.getList = function (userId) {
    return new Promise((resolve, reject) => {
       Promo.find({'userId': userId}, (err, result) => {
        if(err){
            reject(err);
        }else{
            var list = [];
            if(result){
                result.forEach((item) => {
                    list.push({
                        id: String(item._id),
                        name: item.name,
                        desc: item.desc,
                        terms: item.terms,
                        startAt: item.startAt,
                        finishAt: item.finishAt,
                        image: item.image,
                        userId: item.userId,
                        maxUsers: item.maxUsers,
                        maxWinners: item.maxWinners,
                        totalUsers: item.users.length,
                        totalWinners: item.winners.length
                    });
                });
            }
            resolve(list);
        }
    });
    });
};

dao.getListFollowing = function (userId) {
    return new Promise((resolve, reject) => {
       Promo.find({'users': userId}, (err, result) => {
        if(err){
            reject(err);
        }else{
            var list = [];
            if(result){
                result.forEach((item) => {
                    list.push({
                        id: String(item._id),
                        name: item.name,
                        desc: item.desc,
                        terms: item.terms,
                        startAt: item.startAt,
                        finishAt: item.finishAt,
                        image: item.image,
                        userId: item.userId,
                        maxUsers: item.maxUsers,
                        maxWinners: item.maxWinners,
                        totalUsers: item.users.length,
                        totalWinners: item.winners.length
                    });
                });
            }
            resolve(list);
        }
    });
    });
};

dao.add = function (model) {    
    return new Promise((resolve, reject) => {
        let doc = {
            name: model.name,
            desc: model.desc,
            terms: model.terms,
            image: model.image,
            startAt: model.startAt,
            finishAt: model.finishAt,
            userId: model.userId,
            createdAt: moment().toDate(),
            modifiedAt: moment().toDate()
        };

        if(model.id) doc._id = model.id;

        Promo.create(doc, (err, result) => {
            if(err){
                reject(err);
            }else{
                resolve(result._id);
            }
        });
    });
};

dao.update = function (model) {   
    return new Promise((resolve, reject) => {
        let doc = {
            name: model.name,
            desc: model.desc,
            terms: model.terms,
            startAt: model.startAt,
            finishAt: model.finishAt,
            modifiedAt: moment().toDate()
        };

        if(model.image) doc.image = model.image;

        Promo.findByIdAndUpdate(model.id, doc, (err, result) => {
            if(err){
                reject(err);
            }else{
                resolve(result._id);
            }
        });
    });
};

dao.hasUser = function (id, userId) {
    return new Promise((resolve, reject) => {
       Promo.findOne({'_id': id, 'users': {$in: [userId]} }, '_id', (err, result) => {        
            if(err){
                reject(err);
            }else{
                resolve((result) ? true : false);
            }
       });

    });
};

dao.hasWinner = function (id, userId) {
    return new Promise((resolve, reject) => {
       Promo.findOne({'_id': id, 'winners': {$in: [userId]} }, '_id', (err, result) => {        
            if(err){
                reject(err);
            }else{
                resolve((result) ? true : false);
            }
       });

    });
};

dao.addUser = function (id, userId) {
    return new Promise((resolve, reject) => {
        Promo.update({_id: id}, { $addToSet: { users: userId }}, (err, result) => {
            if(err){
                return reject(err);
            }
            User.update({_id: userId}, { $addToSet: { promos: id }}, (err, result) => {
                if(err){
                    return reject(err);
                }
                resolve(true);
            });
        });
    });
};

dao.removeUser = function (id, userId) {
    return new Promise((resolve, reject) => {
        Promo.update({_id: id}, { $pull: { users: userId }}, (err, result) => {
            if(err){
                return reject(err);
            }
            User.update({_id: userId}, { $pull: { promos: id }}, (err, result) => {
                if(err){
                    return reject(err);
                }
                resolve(true);
            });
        });
    });
};

dao.addWinners = function (id, userIds) {
    return new Promise((resolve, reject) => {
        Promo.update({_id: id}, { $addToSet: { winners: userIds }}, (err, result) => {
            if(err){
                return reject(err);
            }
            User.update({_id: {$in: userIds}}, { $addToSet: { won: id }}, (err, result) => {
                if(err){
                    return reject(err);
                }
                resolve(true);
            });
        });
    });
};

dao.removeWinner = function (id, userId) {
    return new Promise((resolve, reject) => {
        Promo.update({_id: id}, { $pull: { winners: userId }}, (err, result) => {
            if(err){
                return reject(err);
            }
            User.update({_id: userId}, { $pull: { won: id }}, (err, result) => {
                if(err){
                    return reject(err);
                }
                resolve(true);
            });
        });
    });
};

dao.getTotalUsers = function (id) {
    return new Promise((resolve, reject) => {
        Promo.findById(id, (err, result) => {
            if(err){
                return reject(err);
            }
            if(!result){
                return resolve(0);
            }            
            resolve(result.users.length);
        });
    });
};

dao.getTotalWinners = function (id) {
    return new Promise((resolve, reject) => {
       Promo.findById(id, (err, result) => {
            if(err){
                return reject(err);
            }
            if(!result){
                return resolve(0);
            }            
            resolve(result.winners.length);
       });
    });
};

dao.getUsers = function (id) {
    return new Promise((resolve, reject) => {
       Promo.findById(id, (err, result) => {
            if(err){
                return reject(err);
            }
            if(!result){
                return resolve([]);
            }
            resolve(result.users);
        });
    });
};

dao.getWinners = function (id) {
    return new Promise((resolve, reject) => {
       Promo.findById(id, (err, result) => {
            if(err){
                return reject(err);
            }
            if(!result){
                return resolve([]);
            }
            resolve(result.winners);
        });
    });
};

module.exports = dao;