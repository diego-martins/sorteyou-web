import Vue from 'vue'
import Vuex from 'vuex'
import authUtils from '@/utils/auth-utils'
import {version} from '../../package.json'

Vue.use(Vuex)

const state = {
    version: '',
    token: '',
    user: null
}

const mutations = {
      setToken (state, value) {
        state.token = value
      },
      setUser (state, value) {
        state.user = value
      },
      init(state) {        
        if(localStorage.getItem('store')){
          let store = JSON.parse(localStorage.getItem('store'))
          if(store.version == version){
            this.replaceState(Object.assign(state, store))
          }else{
            state.version = version
          }
        }
      } 
}

const store = new Vuex.Store({  
  state,
  mutations
})

store.subscribe((mutation, state) => {
  let store = {
    version: state.version,
    user: state.user,
    token: state.token
  }
  localStorage.setItem('store', JSON.stringify(store))
})

export default store