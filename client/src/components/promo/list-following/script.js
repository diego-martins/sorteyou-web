import { api } from '@/utils/http-utils'

export default {
    name: 'listPromoFollowing',
    data: function () {
        return {
            dataList: []
        }
    },
    created: function () {
        api.get('/admin/promo/following').then(res => {
            if (res.data.success) {
                this.dataList = res.data.data
            }
        }).catch(err => {
            console.error(err)
        })
    }
}