import { api } from '@/utils/http-utils'
import moment from 'moment'

export default {
  name: 'AddPromo',
  data: function () {
    return {
      form: {
        name: '',
        desc: '',
        terms: '',
        startAt: '',
        finishAt: '',
        file: null
      },
      errors: [],
      loading: true
    }
  },
  computed: {
    labelBtnSubmit: function () {
      return this.loading ? 'Carregando...' : 'Salvar'
    }
  },
  created() {
    this.loading = false
  },
  methods: {
    handleFile: function () {
    },
    hasFileError: function (form) {
      if (!form.id && !form.file) {
        return 'Selecione uma imagem.'
      }
      return undefined
    },
    hasFormErrors: function (form) {
      let errors = []
      let dateFormat = ['YYYY-MM-DD']

      if (!form.name) {
        errors.push('Informe um nome.')
      } else if (form.name.length > 50) {
        errors.push('O campo nome pode ter no máximo 50 caracteres.')
      }

      if (!form.startAt) {
        errors.push('Informe a data inicial.')
      } else if (
        form.startAt.length != 10 ||
        !moment(form.startAt, dateFormat).isValid()
      ) {
        errors.push('A data inicial é inválida.')
      }

      if (!form.finishAt) {
        errors.push('Informe a data final.')
      } else if (
        form.finishAt.length != 10 ||
        !moment(form.finishAt, dateFormat).isValid()
      ) {
        errors.push('A data final é inválida.')
      }

      let fileError = this.hasFileError(form)
      if (fileError) {
        errors.push(fileError)
      }

      if (!form.desc) {
        errors.push('Informe a descrição.')
      } else if (form.desc.length > 500) {
        errors.push(
          'O campo descrição pode ter no máximo 500 caracteres.'
        )
      }

      if (!form.terms) {
        errors.push('Informe as regras.')
      } else if (form.terms.length > 500) {
        errors.push('O campo regras pode ter no máximo 500 caracteres.')
      }

      return errors.length ? errors : undefined
    },
    submitForm: function (e) {
      e.preventDefault()
      let form = this.form
      let errors = this.hasFormErrors(form)

      if (errors) {
        this.errors = errors
        return
      }

      let formData = new FormData()
      formData.append('file', form.file)
      formData.append('name', form.name)
      formData.append('desc', form.desc)
      formData.append('terms', form.terms)
      formData.append('startAt', form.startAt)
      formData.append('finishAt', form.finishAt)

      let config = {
        headers: {
            "Content-Type": "multipart/form-data"
        }
      }

      this.loading = true;
      api
        .post('/admin/promo', formData, config)
        .then(res => {
          if (res.data.success) {
            this.$router.push({ name: 'listPromo' })
          } else {
            this.errors = res.data.data.errors || []
            this.loading = false
          }
        })
        .catch(err => {
          console.error(err)
          this.loading = false
        });
    }
  }
}