import { api } from '@/utils/http-utils'
import moment from 'moment'

export default {
    name: 'PromoDetail',
    data: function () {
        return {
            id: '',
            name: '',
            desc: '',
            terms: '',
            startAt: '',
            endAt: '',
            image: ''
        }
    },
    created: function () {
        this.id = this.$route.params.id
        api.get(`/admin/promo/${this.id}`).then(res => {
            if (res.data.success) {
                var data = res.data.data
                this.name = data.name
                this.desc = data.desc
                this.terms = data.terms
                this.startAt = moment(data.startAt).format('DD/MM/YYYY')
                this.endAt = moment(data.endAt).format('DD/MM/YYYY')
                this.image = data.image
            }else{
                this.$router.push('/ops#404')
            }
        }).catch(err => {
            console.error(err)
            if(err.response.status == 401){
                this.$router.push('/ops#401')
            }else{
                this.$router.push('/ops#500')
            }
        })
    }
}