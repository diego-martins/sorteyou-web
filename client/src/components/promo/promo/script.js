import { api } from '@/utils/http-utils'
import moment from 'moment'

export default {
    name: 'Promo',
    data: function () {
        return {
            id: '',
            name: '',
            desc: '',
            terms: '',
            startAt: '',
            finishAt: '',
            image: '',
            totalUsers: 0,
            totalWinners: 0,
            maxUsers: 0,
            maxWinners: 0,
            isOwner: false,
            isFinished: false,
            isCompeting: false
        }
    },
    created: function () {
        this.id = this.$route.params.id
        api.get(`/promo/${this.id}`).then(res => {
                if (res.data.success) {
                    var data = res.data.data
                    this.name = data.name
                    this.desc = data.desc
                    this.terms = data.terms
                    this.maxUsers = data.maxUsers || 0
                    this.maxWinners = data.maxWinners || 0
                    this.totalUsers = data.totalUsers || 0
                    this.totalWinners = data.totalWinners || 0
                    this.startAt = moment(data.startAt).format('DD/MM/YYYY')
                    this.finishAt = moment(data.finishAt).format('DD/MM/YYYY')
                    this.image = data.image
                    this.isOwner = data.isOwner
                    this.isFinished = data.isFinished

                    api.post(`/promo/${this.id}/has-user`).then(res => {
                        if (res.data.success) {
                            this.isCompeting = res.data.data
                        }
                    })
                }else{
                    this.$router.push('/ops')
                }
            })
            .catch(err => {
                console.error(err)
                if(err.response.status == 404){
                    this.$router.push('/ops#401')
                } else {
                    this.$router.push('/ops#500')
                }
            })

    },
    computed: {
        btnAddText: function () {
            if (this.isFinished) {
                return 'Encerrado'
            }
            else if (this.isCompeting) {
                return 'Participando'
            }
            else if (this.isOwner) {
                return 'Perticipação negada'
            }
            return 'Participar'
        }
    },
    methods: {
        compete: function () {
            const self = this
            api.post(`/promo/${self.id}/add-user`).then(res => {
                    if (res.data.success) {
                        self.isCompeting = true
                        api.get(`/promo/${self.id}/total-users`).then(res => {
                            if (res.data.success) {
                                self.totalUsers = res.data.data
                            }
                        })
                    }
                })
                .catch(err => {
                    console.error(err)
                    if(err.response.status == 401){
                        this.$router.push({name: 'login', query: { t: this.$route.fullPath }})
                    } else {
                        this.$router.push('/ops#500')
                    }
                })
        }
    }
}