import { api } from '@/utils/http-utils'
import moment from 'moment'

export default {
    name: 'EditPromo',
    data: function () {
        return {
            form: {
                id: '',
                name: '',
                desc: '',
                terms: '',
                startAt: '',
                startAtEnabled: true,
                finishAt: '',
                image: '',
                file: null
            },
            errors: [],
            loading: true
        }
    },
    computed: {
        labelBtnSubmit: function () {
            return this.loading ? 'Carregando...' : 'Salvar';
        }
    },
    created: function () {
        this.form.id = this.$route.params.id
        api.get(`/admin/promo/${this.form.id}`).then(res => {
            if (res.data.success) {
                var data = res.data.data
                this.form.name = data.name
                this.form.desc = data.desc
                this.form.terms = data.terms
                this.form.startAt = moment(data.startAt).format('YYYY-MM-DD')
                this.form.finishAt = moment(data.finishAt).format('YYYY-MM-DD')
                this.form.image = data.image
                this.form.file = null
                this.loading = false;
                this.form.startAtEnabled = moment().isBefore(moment(data.startAt))
            }else{
                this.$router.push('/ops#404')
            }
        })
        .catch(err => {
            console.error(err)
            if(err.response.status == 401){
                this.$router.push('/ops#401')
            }else{
                this.$router.push('/ops#500')
            }
        })
    },
    methods: {
        handleFile: function () {
        },
        hasFileError: function (form) {
            if (!form.id && !form.file) {
                return 'Selecione uma imagem.'
            }
            return undefined
        },
        hasFormErrors: function (form) {
            let errors = [];
            let dateFormat = ['YYYY-MM-DD']

            if (!form.name) {
                errors.push('Informe um nome.')
            } else if (form.name.length > 50) {
                errors.push('O campo nome pode ter no máximo 50 caracteres.')
            }

            if (!form.startAt) {
                errors.push('Informe a data inicial.')
            } else if (
                form.startAt.length != 10 ||
                !moment(form.startAt, dateFormat).isValid()
            ) {
                errors.push('A data inicial é inválida.')
            }

            if (!form.finishAt) {
                errors.push('Informe a data final.')
            } else if (
                form.finishAt.length != 10 ||
                !moment(form.finishAt, dateFormat).isValid()
            ) {
                errors.push('A data final é inválida.')
            }

            let fileError = this.hasFileError(form)
            if (fileError) {
                errors.push(fileError)
            }

            if (!form.desc) {
                errors.push('Informe a descrição.')
            } else if (form.desc.length > 500) {
                errors.push(
                    'O campo descrição pode ter no máximo 500 caracteres.'
                )
            }

            if (!form.terms) {
                errors.push('Informe as regras.')
            } else if (form.terms.length > 500) {
                errors.push('O campo regras pode ter no máximo 500 caracteres.')
            }

            return errors.length ? errors : undefined
        },
        submitForm: function (e) {
            e.preventDefault()
            let form = this.form;
            let errors = this.hasFormErrors(form)
            if (errors) {
                this.errors = errors
                return
            }

            let formData = new FormData()
            formData.append('file', form.file)
            formData.append('name', form.name)
            formData.append('desc', form.desc)
            formData.append('terms', form.terms)
            formData.append('startAt', form.startAt)
            formData.append('finishAt', form.finishAt)

            let config = {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }

            this.loading = true
            api
                .put(`/admin/promo/${form.id}`, formData, config)
                .then(res => {
                    if (res.data.success) {
                        this.$router.push({ name: 'listPromo' })
                    } else {
                        this.errors = res.data.data.errors || []
                        this.loading = false
                    }
                })
                .catch(err => {
                    console.error(err)
                    this.loading = false
                })
        }
    }
}