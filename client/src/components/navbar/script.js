import authUtils from '@/utils/auth-utils'
import {config} from '@/utils/firebase-config'

export default {
    name: 'Navbar',
    data: function () {
        return {
            items: [],
            display: !config.production
        }
    },
    computed: {
        authenticated: function () {
            return this.$store.state.token && authUtils.check()
        },
        list: function () {
            const auth = this.authenticated
            this.items.forEach(item => {
                item.display = (item.displayToAuth && auth) || (!item.displayToAuth && !auth)
            })
            return this.items
        }
    },
    created: function () {
        this.items = [
            {
                name: 'listPromo',
                title: 'Minhas promoções',
                displayToAuth: true
            },
            {
                name: 'listPromoFollowing',
                title: 'Participando',
                displayToAuth: true
            },
            {
                name: 'profile',
                title: 'Perfil',
                displayToAuth: true
            },
            {
                name: 'login',
                title: 'Entrar',
                displayToAuth: false
            }
        ]
    }
}