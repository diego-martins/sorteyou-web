import firebase from 'firebase'
import { api } from '@/utils/http-utils'
import authUtils from '@/utils/auth-utils'

export default {
    name: 'Login',
    data: function () {
        return {
            isLoading: false,
            errors: []
        }
    },
    created: function () {
        firebase.auth().getRedirectResult().then(authResult => {
            if(authResult.user){
                this.isLoading = true
                this.getIdToken().then(idTokenResult => {
                    this.login(idTokenResult).then(loginResult => {
                        this.isLoading = false
                        const target = this.$route.query.t || '/'
                        this.$router.push(target)                                   
                    }).catch(err => {
                        this.isLoading = false
                        this.errors.push('Não foi possível realizar o login.')
                    })
                }).catch(err => {
                    this.isLoading = false
                        this.errors.push('Não foi possível realizar o login.')
                        console.error(err)
                }) 
            }
        })
        .catch(err => {
            this.isLoading = false
            this.errors.push('Não foi possível realizar o login com o Google.')
        })
    },
    methods: {
        loginGoogle: function () {
            this.errors = []
            let provider = new firebase.auth.GoogleAuthProvider()
            provider.addScope('https://www.googleapis.com/auth/user.birthday.read')
            provider.setCustomParameters({prompt: 'select_account'})
            firebase.auth().useDeviceLanguage()
            this.isLoading = true            
            firebase.auth().signInWithRedirect(provider)            
        },
        loginGoogle2: function () {
            this.errors = []
            let provider = new firebase.auth.GoogleAuthProvider()
            provider.addScope('https://www.googleapis.com/auth/user.birthday.read')
            provider.setCustomParameters({prompt: 'select_account'})
            firebase.auth().useDeviceLanguage()
            this.isLoading = true            
            //firebase.auth().signInWithRedirect(provider)
            firebase.auth().signInWithPopup(provider).then(authResult => {
                this.getIdToken().then(idTokenResult => {
                    this.login(idTokenResult).then(loginResult => {
                        this.isLoading = false
                        const target = this.$route.query.t || '/'
                        this.$router.push(target)                                   
                    }).catch(err => {
                        this.isLoading = false
                        this.errors.push('Não foi possível realizar o login.')
                    })
                }).catch(err => {
                    this.isLoading = false
                        this.errors.push('Não foi possível realizar o login.')
                        console.error(err)
                })                
            })
                .catch(err => {
                    this.isLoading = false
                    this.errors.push('Não foi possível realizar o login com o Google.')
                })
        },        
        loginFacebook: function () {
            this.errors = []
            let provider = new firebase.auth.FacebookAuthProvider()
            provider.addScope('user_birthday')
            provider.setCustomParameters({prompt: 'select_account'})
            firebase.auth().useDeviceLanguage()
            firebase.auth().signInWithRedirect(provider)
            this.isLoading = true            
        },
        login: function (idToken) {
            const self = this
            return new Promise((resolve, reject) => {
                api.post('/auth', {'gfaToken': idToken}).then(result => {
                    if(result.data.success){
                        var data = result.data.data
                        this.$store.commit('setToken', data.token)
                        this.$store.commit('setUser', data.user)
                        resolve(true)
                    }else{
                        authUtils.logout()
                        this.$store.commit('setToken', '')
                        this.$store.commit('setUser', null)
                        resolve(false)
                    }
                }).catch(err => {
                    reject(err)
                })
            })
        },
        getIdToken: function () {
            return new Promise((resolve, reject) => {
                var user = firebase.auth().currentUser
                if (user) {
                    user.getIdToken(true).then(idToken => {
                        resolve(idToken)    
                    }).catch(err => {
                        reject(err)                        
                    })
                } else {
                    reject('Usuário não autenticado!')
                }   
            })
        }

    }
}