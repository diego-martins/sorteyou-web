import firebase from 'firebase'
import authUtils from '@/utils/auth-utils'

export default {
    name: 'Profile',
    data: function () {
        return {
            photo: '',
            userId: '',
            name: '',
            email: '',
            user: {}
        }
    },
    created: function () {
        this.user = this.$store.state.user
        if (this.user) {
            this.name = this.user.name
            this.email = this.user.email
            this.picture = this.user.picture
            this.userId = this.user.uid
        }
    }, 
    methods: {
        logout: function () {
            firebase.auth().signOut().then(() => {
                this.$store.commit('setToken', '')
                this.$store.commit('setUser', null)
                this.$router.push({ name: 'home' })    
            })
        }
    }
}