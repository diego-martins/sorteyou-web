import RandNumber from '@/components/rand-number'
import RandAlphabet from '@/components/rand-alphabet'
import RandWords from '@/components/rand-words'
import CEF from '@/components/rand-cef'

export default {
    name: 'Home',
    data: function () {
        return {
            tabIndex: 0,
            hashs: ['#numbers', '#alphabet', '#names', '#cef']
        }
    },
    created: function () {
        const hash = this.$route.hash || this.hashs[0]
        for(let i=0; i<this.hashs.length; i++){
            if(this.hashs[i] == hash){
                this.tabIndex = i
            }
        }
    },
    components: {
        'rand-number': RandNumber,
        'rand-alphabet': RandAlphabet,
        'rand-words': RandWords,
        'rand-cef': CEF
    }
}