export default {
    name: 'CEF',
    data: function () {
        return {
            qtdDefault: 1,
            minValDefault: 1,
            maxValDefault: 100,
            minValRequired: 0,
            minQtdRequired: 1,
            maxValRequired: 10000,
            maxQtdRequired: 100,
            minVal: 0,
            maxVal: 0,
            qtd: 0,
            isLoading: false,
            errors: [],
            results: [],
            selectedType: 0,
            optionsType: [
                {text: 'Mega Sena', value: 0, min: 1, max: 60, qtd: [6,7,8,9,10,11,12,13,14,15]},
                {text: 'Loto Fácil', value: 1, min: 1, max: 25, qtd: [15,16,17,18]},
                {text: 'Quina', value: 2, min: 1, max: 80, qtd: [5,6,7,8,9,10,11,12,13,14,15]}
            ],              
            optionsQtd: []
        }
    },
    created: function () {
        this.selected = 0
    },
    computed: {
        selected: {
            get: function () {
                return this.selectedType
            },
            set: function (value) {
                this.selectedType = value
                const option = this.optionsType[value]
                this.minVal = option.min
                this.maxVal = option.max
                this.optionsQtd = option.qtd
                this.qtd = option.qtd[0]
            }
        },
        resultText: function () {
            let _return = ''
            let count = this.results.length
            for (let i = 0; i < count; i++) {
                if (_return) {
                    _return += ' - '
                }
                _return += this.results[i]
            }
            return _return
        }
    },
    methods: {
        isValid: function () {
            this.errors = []
            this.results = []
            if(this.minVal == this.maxVal){
                this.errors.push(`O valor inicial não pode ser igual o valor final.`)
            }

            if (this.minVal < this.minValRequired || this.minVal > this.maxValRequired) {
                this.errors.push(`Informe um valor de ${this.minValRequired} à ${this.maxValRequired} para o número inicial.`)
            }

            if (this.maxVal < this.minValRequired || this.maxVal > this.maxValRequired) {
                this.errors.push(`Informe um valor de ${this.minValRequired} à ${this.maxValRequired} para o número final.`)
            }

            if(this.errors.length == 0){
                if(this.minVal > this.maxVal){
                    this.errors.push('O valor inicial não pode ser maior que o valor final.')
                }
                else if(this.qtd > this.maxVal){
                    this.errors.push('A quantidade não pode ser maior que o valor final.')
                }
                else if (this.qtd < this.minQtdRequired || this.qtd > this.maxQtdRequired) {
                    this.errors.push(`Informe uma quantidade de ${this.minQtdRequired} à ${this.maxQtdRequired}.`)
                }    
            }

            return (this.errors.length == 0)
        },
        rand: function () {
            this.isLoading = true
            setTimeout(()=>{
                if (this.isValid()) {
                    this.results = []
                    while (this.results.length < this.qtd) {
                        const value = this.getRandom(this.minVal, this.maxVal)
                        let has = false
                        for (let i = 0; i < this.results.length; i++) {
                            if (this.results[i] == value) {
                                has = true
                                break
                            }
                        }
                        if (!has) {
                            this.results.push(value)
                        }
                    }
                }
                this.isLoading = false    
            }, 500)
        },
        getRandom: function (min, max) {
            min = parseInt(min)
            max = parseInt(max)
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }
}