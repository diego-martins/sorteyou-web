export default {
    name: 'RandAlphabet',
    data: function () {
        return {
            qtd: 1,
            minRequired: 1,
            maxRequired: 26,
            isLoading: false,
            errors: [],
            results: [],
            options: [],
            values: []
        }
    },
    created: function () {
        for(let i=65; i<=90; i++){
            this.values.push(String.fromCharCode(i))
            this.options.push(String.fromCharCode(i))
        }
        this.setDefault()
    },
    computed: {
        resultText: function () {
            let _return = ''
            let count = this.results.length
            for (let i = 0; i < count; i++) {
                if (_return) {
                    _return += ' - '
                }
                _return += this.results[i]
            }
            return _return
        }
    },
    methods: {
        setDefault: function () {
            this.minRequired = 1
            this.maxRequired = this.values.length
        },
        isValid: function () {
            this.errors = []
            this.results = []
            this.setDefault()

            if(this.values == 0){
                this.errors.push(`Você precisa selecionar ao menos uma letra.`)
            }
            else if (this.qtd < this.minRequired || this.qtd > this.maxRequired) {
                this.errors.push(`Informe uma quantidade de ${this.minRequired} à ${this.maxRequired}.`)
            }    

            return (this.errors.length == 0)
        },
        rand: function () {
            this.isLoading = true
            setTimeout(()=>{
                if (this.isValid()) {
                    this.results = []
                    
                    while (this.results.length < this.qtd) {
                        const index = this.getRandom(0, this.values.length - 1)
                        const value = this.values[index]
                        let has = false
                        for (let i = 0; i < this.results.length; i++) {
                            if (this.results[i] == value) {
                                has = true
                                break
                            }
                        }
                        if (!has) {
                            this.results.push(value)
                        }
                    }
                    
                }
                this.isLoading = false    
            }, 500)
        },
        getRandom: function (min, max) {
            min = parseInt(min)
            max = parseInt(max)
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }
}