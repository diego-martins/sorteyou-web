export default {
    name: 'RandNumber',
    data: function () {
        return {
            qtdDefault: 1,
            minValDefault: 1,
            maxValDefault: 100,
            minValRequired: 0,
            minQtdRequired: 1,
            maxValRequired: 10000,
            maxQtdRequired: 100,
            minVal: 0,
            maxVal: 0,
            qtd: 0,
            isLoading: false,
            errors: [],
            results: []
        }
    },
    created: function () {
        this.setDefault()
    },
    computed: {
        resultText: function () {
            let _return = ''
            let count = this.results.length
            for (let i = 0; i < count; i++) {
                if (_return) {
                    _return += ' - '
                }
                _return += this.results[i]
            }
            return _return
        }
    },
    methods: {
        setDefault: function () {
            if (!this.minVal || this.minVal < 0) {
                this.minVal = this.minValDefault
            }
            if (!this.maxVal) {
                this.maxVal = this.maxValDefault
            }
            if (!this.qtd) {
                this.qtd = this.qtdDefault
            }
            this.maxQtdRequired = this.maxVal - this.minVal + 1
        },
        isValid: function () {
            this.errors = []
            this.results = []
            this.setDefault()
            
            if(this.minVal == this.maxVal){
                this.errors.push(`O valor inicial não pode ser igual o valor final.`)
            }

            if (this.minVal < this.minValRequired || this.minVal > this.maxValRequired) {
                this.errors.push(`Informe um valor de ${this.minValRequired} à ${this.maxValRequired} para o número inicial.`)
            }

            if (this.maxVal < this.minValRequired || this.maxVal > this.maxValRequired) {
                this.errors.push(`Informe um valor de ${this.minValRequired} à ${this.maxValRequired} para o número final.`)
            }

            if(this.errors.length == 0){
                if(this.minVal > this.maxVal){
                    this.errors.push('O valor inicial não pode ser maior que o valor final.')
                }
                else if(this.qtd > this.maxVal){
                    this.errors.push('A quantidade não pode ser maior que o valor final.')
                }
                else if (this.qtd < this.minQtdRequired || this.qtd > this.maxQtdRequired) {
                    this.errors.push(`Informe uma quantidade de ${this.minQtdRequired} à ${this.maxQtdRequired}.`)
                }    
            }

            return (this.errors.length == 0)
        },
        rand: function () {
            this.isLoading = true
            setTimeout(()=>{
                if (this.isValid()) {
                    this.results = []
                    while (this.results.length < this.qtd) {
                        const value = this.getRandom(this.minVal, this.maxVal)
                        let has = false
                        for (let i = 0; i < this.results.length; i++) {
                            if (this.results[i] == value) {
                                has = true
                                break
                            }
                        }
                        if (!has) {
                            this.results.push(value)
                        }
                    }
                }
                this.isLoading = false    
            }, 500)
        },
        getRandom: function (min, max) {
            min = parseInt(min)
            max = parseInt(max)
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }
}