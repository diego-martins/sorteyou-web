import Vue from 'vue'
import Router from 'vue-router'
import jwtDecode from 'jwt-decode'
import store from '@/store'

import MasterPage from '@/components/master-page'

import Home from '@/components/home'
import Login from '@/components/login'
import Profile from '@/components/profile'
import Promo from '@/components/promo/promo'
import ListPromo from '@/components/promo/list'
import ListPromoFollowing from '@/components/promo/list-following'
import AddPromo from '@/components/promo/add'
import EditPromo from '@/components/promo/edit'
import PromoDetail from '@/components/promo/detail'
import authUtils from '@/utils/auth-utils'
import Ops from '@/components/ops'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: MasterPage,
      children: [
        {
          name: 'home',
          path: '/',
          component: Home
        },
        {
          name: 'login',
          path: '/login',
          component: Login
        },
        {
          name: 'profile',
          path: '/profile',
          component: Profile,
          meta: {
            auth: true
          }
        },
        {
          name: 'promo',
          path: '/promo/:id',
          component: Promo
        }

      ]
    },
    {
      path: '/',
      component: MasterPage,
      children: [        
        {
          name: 'listPromo',
          path: '/list-promo',
          component: ListPromo,
          meta: {
            auth: true
          }
        },
        {
          name: 'listPromoFollowing',
          path: '/list-promo-following',
          component: ListPromoFollowing,
          meta: {
            auth: true
          }
        },
        {
          name: 'addPromo',
          path: '/add-promo',
          component: AddPromo,
          meta: {
            auth: true
          }
        },
        {
          name: 'editPromo',
          path: '/edit-promo/:id',
          component: EditPromo,
          meta: {
            auth: true
          }
        },
        {
          name: 'promoDetail',
          path: '/promo-detail/:id',
          component: PromoDetail,
          meta: {
            auth: true
          }
        }
      ]
    },
    {
      path: '/',
      component: MasterPage,
      children: [
        {
          name: 'ops',
          path: '*',
          component: Ops
        }  
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.auth && !authUtils.check()) {
    return next({
      name: 'login',
      query: { t: to.fullPath }
    })
  }
  next()  
})

export default router
