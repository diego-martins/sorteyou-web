
const env = {
    dev: {
        production: false,
        apiKey: "",
        authDomain: "",
        databaseURL: "",
        projectId: "",
        storageBucket: "",
        messagingSenderId: ""    
    },
    prod: {
        production: true,
        apiKey: "",
        authDomain: "",
        databaseURL: "",
        projectId: "",
        storageBucket: "",
        messagingSenderId: ""    
    }
}

export const config = (process.env.NODE_ENV == 'production') ? env['prod'] : env['dev']
