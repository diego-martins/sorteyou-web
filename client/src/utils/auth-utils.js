import jwtDecode from 'jwt-decode'
import moment from 'moment'

export default {        
    check: function () {
        if(!localStorage.getItem('store')){
            return false
        }
        const store = JSON.parse(localStorage.getItem('store'))
        const token = store.token||''
        if(!token){
            return false
        }
        try{
            const decoded = jwtDecode(token||'')
            const exp = (decoded && decoded.exp) ? decoded.exp : 0
            const expireAt = moment(exp * 1000)
            if(moment().isAfter(expireAt)){
                return false
            }
        }catch(ex){
            console.error(ex)
            return false
        }
        return true
    }
}