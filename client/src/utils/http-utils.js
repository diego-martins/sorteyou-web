import axios from 'axios'
import authUtils from '@/utils/auth-utils'
import store from '@/store'

export const api = axios.create({
    //baseURL: 'http://localhost:3000/api'
    baseURL: 'http://192.168.15.12:3000/api'
});

api.interceptors.request.use(config => {
        const token = store.state.token
        if(token){
            config.headers.Authorization = `Bearer ${token}`
        }
        return config
    }, err => {
        return Promise.reject(err)
    }
)
