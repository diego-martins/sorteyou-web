import Vue from 'vue'
import App from '@/components/app'
import router from '@/router'
import { config } from '@/utils/firebase-config'
import firebase from 'firebase'
import store from '@/store'
import BootstrapVue from 'bootstrap-vue'
import VueLazyload from 'vue-lazyload'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueLazyload, {
  preLoad: 1.3,
  //error: '/static/images/no-image.png',
  loading: '/static/images/loading.gif',
  attempt: 1,
})
firebase.initializeApp(config)

new Vue({
  el: '#app',
  store,
  router,
  beforeCreate: function () {
    this.$store.commit('init')
  },
  components: { App },
  template: '<App/>'
})
